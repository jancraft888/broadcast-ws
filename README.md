# broadcast-ws
BroadcastWS - WebSocket event-based broadcast service.

## Public Instance
wss://relay.jdevstudios.es/

## Structure

> 🛈 All packets are in the `JSON` format

Once a WebSocket connection is open, the first `Client->Server` packet must be the **Handshake** packet. Every single packet sent *after* the **Handshake** packet must be a **Data** packet.

### Handshake packet
```json
{
    "clientID": "UUID v4 Client ID",
    "listens": [
        
    ]
}
```

### Data packet
```json
{
    "event": "Event ID in the format specified below",
}
```

### *Target*s and *Sender*s
When sending a packet, you can specify the `$target` property to only send the packet to the specified Client ID.
When receiving a packet, the `$sender` property is set by the server to the Client ID of the sender.

### Event ID format
The **Event ID** in the **Data** packet must be in the format specified below.

**NAMESPACE**`:CATEGORY`*/EVENT*

## JS Library
There's also a JS library found in this repo at `lib/broadcastws.min.js` that wraps the WebSocket connection and adds some new methods.

`BroadcastWS(clientID, listens)` -> Creates a wrapped WebSocket object, send the handshake packet, and returns it.

On the wrapped WebSocket object there's a new method:

`.event(eventID, data)` -> Sends the *data* JSON object with the *event* property set.
