FROM node:16

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm ci

COPY . .

EXPOSE 8080
ENV BWS_PORT=8080
CMD [ "node", "app.js" ]
